"""Create posts table

Revision ID: c15869f05d08
Revises: 
Create Date: 2022-07-19 16:03:41.305999

"""
import sqlalchemy as sa
from alembic import op

# revision identifiers, used by Alembic.
revision = "c15869f05d08"
down_revision = None
branch_labels = None
depends_on = None


def upgrade() -> None:
    op.create_table(
        "posts",
        sa.Column("id", sa.Integer(), nullable=False, primary_key=True),
        sa.Column("title", sa.String(), nullable=False),
        sa.Column("content", sa.String(), nullable=False),
        sa.Column("published", sa.Boolean(), server_default="FALSE", nullable=False),
        sa.Column(
            "timestamp",
            sa.TIMESTAMP(timezone=True),
            server_default=sa.text("now()"),
            nullable=False,
        ),
    )
    pass


def downgrade() -> None:
    op.drop_table("posts")
