"""Create users table

Revision ID: 99b244ed42b9
Revises: c15869f05d08
Create Date: 2022-07-19 17:18:15.720269

"""
import sqlalchemy as sa
from alembic import op

# revision identifiers, used by Alembic.
revision = "99b244ed42b9"
down_revision = "c15869f05d08"
branch_labels = None
depends_on = None


def upgrade() -> None:
    op.create_table(
        "users",
        sa.Column("id", sa.Integer(), nullable=False, primary_key=True),
        sa.Column("email", sa.String(), nullable=False, unique=True),
        sa.Column("password", sa.String(), nullable=False),
        sa.Column(
            "timestamp",
            sa.sql.sqltypes.TIMESTAMP(timezone=True),
            server_default=sa.sql.expression.text("now()"),
            nullable=False,
        ),
    )


def downgrade() -> None:
    op.drop_table("users")
