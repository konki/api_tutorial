"""Add column user_id to posts table

Revision ID: 6edca35fc176
Revises: 99b244ed42b9
Create Date: 2022-07-19 17:29:33.455334

"""
import sqlalchemy as sa
from alembic import op

# revision identifiers, used by Alembic.
revision = "6edca35fc176"
down_revision = "99b244ed42b9"
branch_labels = None
depends_on = None


def upgrade() -> None:
    op.add_column("posts", sa.Column("user_id", sa.Integer(), nullable=False))
    op.create_foreign_key(
        None,
        "posts",
        "users",
        ["user_id"],
        ["id"],
        onupdate="CASCADE",
        ondelete="CASCADE",
    )


def downgrade() -> None:
    op.drop_constraint(None, "posts", type_="foreignkey")
    op.drop_column("posts", "user_id")
