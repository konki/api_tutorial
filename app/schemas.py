# pylint: disable=no-name-in-module
# pylint: disable=no-self-argument

from datetime import datetime
from typing import Optional

from pydantic import BaseModel, EmailStr, conint

# USERS


class BaseUser(BaseModel):
    """Basic user"""

    email: EmailStr


class UserCreate(BaseUser):
    """User used during registration"""

    password: str


class UserLogin(BaseUser):
    """User used during login"""

    password: str


class UserResponse(BaseUser):
    """User returned when querying for user"""

    id: int
    timestamp: datetime

    class Config:
        orm_mode = True


# POSTS


class PostBase(BaseModel):
    """Basic post"""

    title: str
    content: str
    published: bool = False


class PostCreate(PostBase):
    """Post used during post creation"""


class PostResponse(PostBase):
    """Basic post returned when querying for post(s)"""

    id: int
    timestamp: datetime
    user_id: int
    user: UserResponse

    class Config:
        orm_mode = True


class PostResponseVote(BaseModel):
    """Post returned when querying for post(s) including vote count"""

    Post: PostResponse
    votes: Optional[int] = 0

    class Config:
        orm_mode = True


# VOTE


class VoteBase(BaseModel):
    """Basic vote"""

    post_id: int
    direction: conint(le=1, ge=-1) = 0


class VoteCreate(VoteBase):
    """Vote used when registering vote"""


class VoteResponse(VoteBase):
    """Vote used when querying vote(s)"""

    user_id: int


# JWT TOKEN


class Token(BaseModel):
    access_token: str
    token_type: str


class TokenData(BaseModel):
    id: Optional[str]
