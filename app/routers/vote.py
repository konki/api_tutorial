from fastapi import APIRouter, Depends, HTTPException, status
from sqlalchemy.orm import Session

from .. import models, oauth2, schemas
from ..database import get_db

router = APIRouter(prefix="/vote", tags=["Vote"])


@router.post("/", status_code=status.HTTP_201_CREATED)
def vote(
    new_vote: schemas.VoteCreate,
    db: Session() = Depends(get_db),
    current_user=Depends(oauth2.get_current_user),
):
    """Let current user vote on a given post"""
    post = (
        db.query(models.Post)
        .filter(models.Post.id == new_vote.post_id, models.Post.published is True)
        .first()
    )

    if not post:
        raise HTTPException(
            status_code=status.HTTP_404_NOT_FOUND,
            detail=f"Post with id {new_vote.post_id} was not found",
        )

    vote_query = db.query(models.Vote).filter(
        models.Vote.post_id == new_vote.post_id, models.Vote.user_id == current_user.id
    )
    found_vote = vote_query.first()

    if not vote:
        db.add(models.Vote(**new_vote.dict(), user_id=current_user.id))
    elif found_vote.direction == new_vote.direction:
        raise HTTPException(
            status_code=status.HTTP_409_CONFLICT,
            detail=f"User with id {current_user.id} has already voted on post with id {new_vote.post_id} with direction {new_vote.direction}.",
        )
    else:
        updated_vote = schemas.VoteResponse(**new_vote.dict(), user_id=current_user.id)
        vote_query.update(updated_vote.dict(), synchronize_session=False)
    db.commit()

    return {"message": "Vote deliviered successfully"}
