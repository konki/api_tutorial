from passlib.context import CryptContext

pwd_context = CryptContext(schemes=["bcrypt"], deprecated="auto")


def hash(pwd: str) -> str:
    """Hash a password"""
    return pwd_context.hash(pwd)


def verify(plain_password: str, hashed_password: str) -> bool:
    """Verify that hashing given plaintext password is equal to hashed password stored in db"""
    return pwd_context.verify(plain_password, hashed_password)
